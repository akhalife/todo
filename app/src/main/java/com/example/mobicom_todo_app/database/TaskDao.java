package com.example.mobicom_todo_app.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.mobicom_todo_app.model.Task;

import java.util.List;

@Dao
public interface TaskDao{

    @Insert
    void addTask(Task task);

    @Update
    void updateTask(Task task);

    @Query("select * from task_table")
    LiveData<List<Task>> getAllTasks();

    @Query("delete from task_table")
    void deleteAllTasks();

    @Query("delete from task_table where mid=:TaskId")
    void deleteSingleTask(Long TaskId);

    @Insert
    void insertTasks(List<Task> tasks);

    // Room maps booleans to 0 = false and 1 = true
    @Query("delete from task_table where done=1")
    void deleteFinishedTasks();

}
