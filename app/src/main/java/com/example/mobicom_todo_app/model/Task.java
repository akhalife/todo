package com.example.mobicom_todo_app.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Date;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


/**
 * Created by thorsten on 21.03.20.
 * updated by mvoigt and akalife
 */

@Entity(tableName = "task_table")
public class Task implements Parcelable {


    @PrimaryKey(autoGenerate = true)
    private Long mid;

    @ColumnInfo(name = "shot_name")
    private String mShortName;

    @ColumnInfo(name = "description")
    private String mDescription = "";

    @ColumnInfo(name = "due_date")
    private Date dueDate;

    @ColumnInfo(name = "done")
    private boolean mDone = false;

    @ColumnInfo(name = "tasklist_id")
    private Long taskListId = null;

    public Task(String shortName) {
        this.mShortName = shortName;
    }

    @Ignore
    public Task(Long mid, String mShortName, String mDescription, Date dueDate, boolean mDone, Long taskListId) {
        this.mid = mid;
        this.mShortName = mShortName;
        this.mDescription = mDescription;
        this.dueDate = dueDate;
        this.mDone = mDone;
        this.taskListId = taskListId;
    }

    protected Task(Parcel in) {
        mid = readLongFromParcel(in);
        mShortName = in.readString();
        mDescription = in.readString();
        Long timeMillis = readLongFromParcel(in);
        if (timeMillis != null) {
            dueDate = new Date(timeMillis);
        }
        mDone = in.readByte() != 0;
        taskListId = readLongFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        writeLongToParcel(mid, dest);
        dest.writeString(mShortName);
        dest.writeString(mDescription);
        writeLongToParcel(dueDate != null ? dueDate.getTime() : null, dest);
        dest.writeByte((byte) (mDone ? 1 : 0));
        writeLongToParcel(taskListId, dest);
    }

    private void writeLongToParcel(Long data, Parcel dest) {
        if (data == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(data);
        }
    }

    private Long readLongFromParcel(Parcel in) {
        if (in.readByte() == 0) {
            return null;
        } else {
            return in.readLong();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    /**
     * Creates a copy to prevent pointing on
     *
     * @return The copy of the Task instance
     */
    public Task copyTask() {
        Task copy = new Task(this.mShortName);
        copy.dueDate = this.dueDate;
        copy.mDescription = this.mDescription;
        copy.mid = this.mid;
        copy.mDone = this.mDone;
        copy.taskListId = this.taskListId;
        return copy;
    }

    public Long getMid() {
        return mid;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }

    public String getShortName() {
        return mShortName;
    }

    public void setShortName(String shortName) {
        this.mShortName = shortName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public boolean isDone() {
        return mDone;
    }

    public void setDone(boolean done) {
        this.mDone = done;
    }

    public Date getDueDate() {
        return this.dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Long getTaskListId() {
        return taskListId;
    }

    public void setTaskListId(Long taskListId) {
        this.taskListId = taskListId;
    }

    @NonNull
    @Override
    public String toString() {
        return mShortName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Task) {
            return this.getMid() == ((Task) obj).getMid();
        }
        return false;
    }
}
