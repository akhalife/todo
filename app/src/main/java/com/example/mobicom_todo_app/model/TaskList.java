package com.example.mobicom_todo_app.model;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class TaskList {

    // simple ID generator
    private static Long MAX_ID = 0L;

    private final Long mListId;
    private String mListName;
    // represented by their ids
    private final ArrayList<Task> mTasks;

    public TaskList(String mListName) {
        this.mListId = MAX_ID++;
        this.mListName = mListName;
        mTasks = new ArrayList<>();
    }

    /**
     * Get the id of the taskList
     * @return an integer representing this list
     */
    public Long getListId() {
        return mListId;
    }

    /**
     * Get the Name of the taskList
     * @return the name of this list
     */
    public String getListName() {
        return mListName;
    }

    /**
     * Set the name of a taskList
     * @param mListName The name of the list
     */
    public void setListName(String mListName) {
        this.mListName = mListName;
    }

    /**
     * Get all tasks of a taskList
     * @return all tasks associated with this list
     */
    public ArrayList<Task> getTasks() {
        return mTasks;
    }

    /**
     * Add a single task to the taskList. Adds an association with the list too
     * @param mTask the id of the task to add
     */
    public void addTask(Task mTask) {
        mTask.setTaskListId(this.mListId);
        this.mTasks.add(mTask);
    }

    /**
     * Remove a single Task from the taskList. Removes the association with its list too.
     * @param mTask The id of the task to remove
     * @return The removed Task
     */
    public Task removeTask(Task mTask) {
        Task removedTask = this.mTasks.remove(this.mTasks.indexOf(mTask));
        removedTask.setTaskListId(null);
        return removedTask;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof TaskList) {
            return this.getListId() == ((TaskList) obj).getListId();
        }
        return false;
    }
}
