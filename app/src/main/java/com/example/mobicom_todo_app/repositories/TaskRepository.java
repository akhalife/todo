package com.example.mobicom_todo_app.repositories;

import androidx.lifecycle.LiveData;

import java.util.List;

import com.example.mobicom_todo_app.model.Task;
import com.example.mobicom_todo_app.model.TaskList;

/**
 * Created by thorsten on 23.03.20.
 */

public interface TaskRepository {

    // work with tasks
    LiveData<List<Task>> loadTasks();

    void deleteFinishedTasks();

    void deleteSingleTask(Long mTaskId);

    void addSingleTask(Task mTask);

    void addTasks(Task[] mTasks);

    void updateTask(Task mTask);

    // work with taskLists
    List<TaskList> loadTaskLists();

    void addTaskList(TaskList mTaskList);

    void deleteTaskList(int mTaskListId);

    void addTaskToList(Task mTask, TaskList mTaskList);

    void removeTaskFromList(Task mTask, TaskList mTaskList);
}
