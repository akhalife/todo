package com.example.mobicom_todo_app.fragments.taskList;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mobicom_todo_app.R;
import com.example.mobicom_todo_app.adapter.TaskListAdapter;
import com.example.mobicom_todo_app.databinding.TaskListFragmentBinding;
import com.example.mobicom_todo_app.fragments.taskDetails.TaskDetailsFragment;
import com.example.mobicom_todo_app.model.Task;
import com.example.mobicom_todo_app.stores.TaskRepositoryImpl;
import com.example.mobicom_todo_app.util.ViewModelFactory;
import com.google.android.material.snackbar.Snackbar;


public class TaskListFragment extends Fragment implements TaskListAdapter.TaskSelectionListener {

    private TaskListFragmentBinding binding;

    private TaskListAdapter adapter;
    private boolean splitScreen = false;
    TaskDetailsFragment tdf;

    private TaskListViewModel viewModel;

    public static final String TASK_TO_MANAGE_KEY = "TASK_TO_MANAGE";

    // LIFECYCLE
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        ViewModelFactory factory = new ViewModelFactory(TaskRepositoryImpl.getInstance());
        viewModel = new ViewModelProvider(this, factory).get(TaskListViewModel.class);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        binding = TaskListFragmentBinding.inflate(inflater, container, false);

        setAdapter();

        initViewModelObserver();

        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            String eventMessage = getArguments().getString(TaskDetailsFragment.BUNDLE_KEY_EVENT_MESSAGE);
            if (eventMessage != null) {
                Snackbar.make(requireView(), eventMessage, Snackbar.LENGTH_LONG).show();
            }
            // reset arguments to prevent resetting at configuration change
            setArguments(null);
        }

        binding.addTask.setOnClickListener(view1 -> onCreateNewTask());

        FragmentManager manager = getChildFragmentManager();
        tdf = (TaskDetailsFragment) manager
                .findFragmentById(R.id.fragmentDetailsContainer);
        // The tdf is still there after rotating the device, so remove the fragment manually from the
        // fragment manager and teh backstack
        if (binding.fragmentDetailsContainer == null && tdf != null){
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.remove(tdf);
            transaction.commit();
            manager.popBackStack();
            tdf = null;
        }
        this.splitScreen = tdf != null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        unInitViewModelObserver();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_filter_all_tasks:
                viewModel.getFilterState().setValue(TaskListAdapter.FILTER_ALL);
                return true;
            case R.id.menu_filter_all_unfinished:
                viewModel.getFilterState().setValue(TaskListAdapter.FILTER_UNFINISHED);
                return true;
            case R.id.menu_delete_finished:
                if (adapter.getAllItemCount() != 0)
                    this.viewModel.onDeleteFinishedTasks();
                else
                    Snackbar.make(requireView(), getString(R.string.nothing_to_delete), Snackbar.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.menu_delete_one).setVisible(false);
    }

    @SuppressLint("StringFormatInvalid")
    private void initViewModelObserver() {
        viewModel.getDatabaseTasks().observe(getViewLifecycleOwner(), updatedTasks -> {
            // Old value greater than new value --> deleted
            if (adapter.getAllItemCount() > updatedTasks.size()) {
                String message = getString(R.string.deleted_task);
                int diff = adapter.getAllItemCount() - updatedTasks.size();
                if (diff > 1) {
                    message = getString(R.string.deleted_many_tasks, diff);
                }
                if (tdf != null) {
                    tdf.clearTask(updatedTasks);
                }
                Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show();
            }
            adapter.setTasks(updatedTasks);
        });

        viewModel.getFilterState().observe(getViewLifecycleOwner(), state -> {
            adapter.setFilterState(state);
            adapter.setTasks(viewModel.getDatabaseTasks().getValue());
        });
    }

    private void unInitViewModelObserver() {
        viewModel.getDatabaseTasks().removeObservers(getViewLifecycleOwner());
        viewModel.getFilterState().removeObservers(getViewLifecycleOwner());
    }

    // CUSTOM METHODS
    private void setAdapter() {
        RecyclerView recyclerView = binding.tasks;
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new DividerItemDecoration(requireContext(), manager.getOrientation()));

        adapter = TaskListAdapter.getInstance(this);
        binding.setAdapter(adapter);
        recyclerView.setAdapter(adapter);
    }

    private void onCreateNewTask() {
        this.onTaskSelected(new Task(""));
    }

    @Override
    public void onTaskSelected(Task task) {
        if (splitScreen) {
            tdf.setTask(task);
        } else {
            this.navigateToDetails(task);
        }
    }

    @Override
    public void onTaskStatusChange(Task task, boolean status) {
        viewModel.onTaskStatusChange(task, status);
        if (tdf != null) {
            tdf.setTaskStatus(task, status);
        }
    }

    private void navigateToDetails(Task task) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TASK_TO_MANAGE_KEY, task);
        NavHostFragment.findNavController(TaskListFragment.this)
                .navigate(R.id.action_ListFragment_to_DetailsFragment, bundle);
    }

}