package com.example.mobicom_todo_app.fragments.taskDetails;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.example.mobicom_todo_app.R;
import com.example.mobicom_todo_app.databinding.TaskDetailsFragmentBinding;
import com.example.mobicom_todo_app.fragments.taskList.TaskListFragment;
import com.example.mobicom_todo_app.model.Task;
import com.example.mobicom_todo_app.stores.TaskRepositoryImpl;
import com.example.mobicom_todo_app.util.ViewModelFactory;
import com.google.android.material.snackbar.Snackbar;

public class TaskDetailsFragment extends Fragment {

    private TaskDetailsFragmentBinding binding;
    private Calendar cal = Calendar.getInstance();
    private int[] pickerDate;
    private DatePickerDialog dialog;

    private TaskDetailsViewModel viewModel;

    // error and state flags
    private boolean isDateDialogOpen = false;

    // bundle keys
    private static final String BUNDLE_KEY_DATE_DIALOG = "BUNDLE_KEY_DATE_DIALOG";
    private static final String BUNDLE_KEY_PICKER_DATE = "BUNDLE_KEY_PICKER_DATE";
    public static final String BUNDLE_KEY_EVENT_MESSAGE = "BUNDLE_KEY_EVENT_MESSAGE";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        ViewModelFactory factory = new ViewModelFactory(TaskRepositoryImpl.getInstance());
        viewModel = new ViewModelProvider(this, factory).get(TaskDetailsViewModel.class);

        if (getArguments() != null) {
            viewModel.setSplitScreen(false);
            Task task = getArguments().getParcelable(TaskListFragment.TASK_TO_MANAGE_KEY);
            if (task != null) {
                viewModel.setTask(task);
            } else {
                viewModel.setTask(new Task(""));
            }
            // reset arguments to prevent resetting at configuration change
            setArguments(null);
        }
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        binding = TaskDetailsFragmentBinding.inflate(inflater, container, false);

        binding.setLifecycleOwner(this);

        binding.setViewModel(viewModel);

        initViewModelObserver();

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            this.handleInstanceState(savedInstanceState);
        }

        binding.saveTask.setOnClickListener(v -> saveTask());

        this.setupDateDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        unInitViewModelObserver();
        viewModel = null;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(BUNDLE_KEY_DATE_DIALOG, this.isDateDialogOpen);
        if (isDateDialogOpen) {
            DatePicker picker = dialog.getDatePicker();
            outState.putIntArray(BUNDLE_KEY_PICKER_DATE, new int[]{picker.getYear(), picker.getMonth(), picker.getDayOfMonth()});
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (!viewModel.isSplitScreen()) {
            // hide filter in details because nothing to filter
            menu.findItem(R.id.menu_action_filter).setVisible(false);
            menu.findItem(R.id.menu_delete_finished).setVisible(false);
        }
        // if editMode, be able to delete one task
        menu.findItem(R.id.menu_delete_one).setVisible(viewModel.getIsEditMode().getValue());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_delete_one) {
            openDeleteDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void handleInstanceState(Bundle savedInstanceState) {
        this.isDateDialogOpen = savedInstanceState.getBoolean(BUNDLE_KEY_DATE_DIALOG, false);

        if (this.isDateDialogOpen) {
            this.pickerDate = savedInstanceState.getIntArray(BUNDLE_KEY_PICKER_DATE);
        }
    }

    private void initViewModelObserver() {
        viewModel.getNameError().observe(getViewLifecycleOwner(), msg ->
                binding.taskName.setError(msg));

        viewModel.getDateError().observe(getViewLifecycleOwner(), msg ->
                binding.taskDate.setError(msg));

        viewModel.getTaskId().observe(getViewLifecycleOwner(), ID ->
                viewModel.getIsEditMode().setValue(ID != null));

        viewModel.getIsEditMode().observe(getViewLifecycleOwner(), isEditMode -> {
            if (viewModel.isSplitScreen()) {
                requireActivity().invalidateOptionsMenu();
            }
        });
    }

    private void unInitViewModelObserver() {
        viewModel.getNameError().removeObservers(getViewLifecycleOwner());

        viewModel.getDateError().removeObservers(getViewLifecycleOwner());

        viewModel.getTaskId().removeObservers(getViewLifecycleOwner());

        viewModel.getIsEditMode().removeObservers(getViewLifecycleOwner());
    }

    public void setTask(Task taskToManage) {
        viewModel.setTask(taskToManage);
    }

    public void setTaskStatus(Task task, boolean status) {
        if (viewModel.getTaskId().getValue() != null && viewModel.getTaskId().getValue().equals(task.getMid())) {
            viewModel.getTaskDone().setValue(status);
        }
    }

    /**
     * Resets the task of the details fragment, if the task is not in the given List or if the list
     * is null
     * @param taskList The specific TaskList to check
     */
    public void clearTask(List<Task> taskList) {
        if (taskList == null || !taskList.contains(viewModel.getTask())) {
            viewModel.resetTask();
        }
    }

    private DatePickerDialog.OnDateSetListener dateSetListener() {
        return (datePicker, year, month, day) -> {

            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, month);
            cal.set(Calendar.DAY_OF_MONTH, day);

            viewModel.setTaskDate(cal.getTime(), getString(R.string.dateError));
        };
    }

    private void setupDateDialog() {
        binding.taskDate.setOnClickListener(v -> this.openDateDialog());

        // if the dialog was open, open it again with click event on the taskDate
        if (this.isDateDialogOpen) {
            this.openDateDialog();
        }
    }

    private void openDateDialog() {
        int bias = 0;
        if (this.pickerDate == null) {
            // If a date is not set or its invalid
            if (viewModel.getTaskDueDate().getValue() == null || viewModel.getDateError().getValue() != null) {
                cal = Calendar.getInstance();
                bias = 1;
            } else {
                cal.setTime(viewModel.getTaskDueDate().getValue());
            }
        }

        int year = this.pickerDate != null ? this.pickerDate[0] : cal.get(Calendar.YEAR);
        int month = this.pickerDate != null ? this.pickerDate[1] : cal.get(Calendar.MONTH);
        // Get tomorrow if invalid or not set
        int day = this.pickerDate != null ? this.pickerDate[2] : (cal.get(Calendar.DAY_OF_MONTH) + bias);

        this.dialog = new DatePickerDialog(
                getContext(),
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                this.dateSetListener(),
                year, month, day);

        dialog.setOnDismissListener(dialogView -> {
            this.isDateDialogOpen = false;
            this.pickerDate = null;
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        this.isDateDialogOpen = true;
    }

    private void openDeleteDialog() {
        new AlertDialog.Builder(requireContext())
                .setTitle(getString(R.string.delete_task_title))
                .setMessage(getString(R.string.delete_task_message, viewModel.getTaskShortName().getValue()))
                .setPositiveButton(R.string.delete_one, (dialogInterface, i) -> deleteTask())
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel())
                .create().show();
    }

    private boolean isTaskValid() {
        Task task = viewModel.getTask();
        return task.getDueDate() != null && viewModel.getDateError().getValue() == null &&
                task.getShortName().length() > 0;
    }

    private void saveTask() {
        String message;
        if (isTaskValid()) {
            // Ignore warning, this is never null
            if (viewModel.getIsEditMode().getValue()) {
                viewModel.updateTask();
                message = getString(R.string.edited_task);
            } else {
                viewModel.addNewTask();
                message = getString(R.string.created_task);
            }
            hideKeyboard();
            navigateToList(message);
        } else {
            showErrors();
            message = getString(R.string.invalid_task);
        }
        Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG)
                .show();
    }

    private void deleteTask() {
        viewModel.deleteTask();
        navigateToList(getString(R.string.deleted_task));
    }

    private void navigateToList(String message) {
        if (!viewModel.isSplitScreen()) {
            Bundle bundle = new Bundle();
            bundle.putString(BUNDLE_KEY_EVENT_MESSAGE, message);
            NavHostFragment.findNavController(TaskDetailsFragment.this)
                    .navigate(R.id.action_DetailsFragment_to_ListFragment, bundle);
        }
    }

    private void showErrors() {
        if (!isNameValid()) {
            viewModel.getNameError().setValue(getString(R.string.taskNameError));
        }
        if (viewModel.getTaskDueDate().getValue() == null) {
            viewModel.getDateError().setValue(getString(R.string.dateError));
        }
    }

    /**
     * hide the keyboard if possible
     */
    private void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) requireActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(requireActivity().getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            Log.e("HIDE_KEYBOARD", "hideKeyboard: ", e);
        }
    }

    private boolean isNameValid() {
        return binding.taskName.getText().length() > 0;
    }
}