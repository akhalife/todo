package com.example.mobicom_todo_app;

import android.app.Application;
import androidx.room.Room;
import com.example.mobicom_todo_app.database.TaskRoomDatabase;

public class MyTasksApp extends Application{

    public static TaskRoomDatabase appDatabase;

    @Override
    public void onCreate() {
        super.onCreate();

        appDatabase = Room.databaseBuilder(getApplicationContext(), TaskRoomDatabase.class,
                "tasks-db").build();
    }
}
