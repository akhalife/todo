package com.example.mobicom_todo_app.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.mobicom_todo_app.Converter;
import com.example.mobicom_todo_app.model.Task;

@Database(entities = {Task.class}, version = 1, exportSchema = false)
@TypeConverters({Converter.class})
public abstract class TaskRoomDatabase extends RoomDatabase{

    public abstract TaskDao taskDao();
}
