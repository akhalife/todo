package com.example.mobicom_todo_app.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mobicom_todo_app.databinding.TaskListEntryBinding;
import com.example.mobicom_todo_app.model.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a special singleton TaskListAdapter. We just need one instance of this adapter in the
 * TaskListFragment. Another reason to make this to a singleton is, to save the filterState correctly
 */
public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.TaskViewHolder> {

    private static TaskListAdapter instance;

    public static final String FILTER_ALL = "FILTER_ALL";
    public static final String FILTER_UNFINISHED = "FILTER_UNFINISHED";

    private TaskSelectionListener listener;
    private List<Task> filteredTasks;
    private List<Task> allTasks;

    private String filterState = FILTER_ALL;

    // prop to show an empty state of the recycler view. Because the filter function is in this
    // adapter, this prop is in here and not in the viewModel of the taskList
    private final MutableLiveData<Boolean> isListEmpty;

    private TaskListAdapter(TaskSelectionListener listener) {
        this.listener = listener;
        this.filteredTasks = new ArrayList<>();
        this.allTasks = new ArrayList<>();
        isListEmpty = new MutableLiveData<>(true);
    }

    public static synchronized TaskListAdapter getInstance(TaskSelectionListener listener) {
        if (instance == null) {
            instance = new TaskListAdapter(listener);
        } else {
            // if the listener is different, change it
            if (listener != instance.listener)
                instance.listener = listener;
        }
        return instance;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setTasks(List<Task> tasks){
        allTasks = tasks;
        // Make copy of tasks to avoid overwriting the tasks of the given taskList in the viewModel.
        // If the given List(of viewModel) is changed, it can not be unfiltered without changing
        // the database and cause a reload
        this.filteredTasks = filterTasks(new ArrayList<>(tasks));
        isListEmpty.setValue(this.filteredTasks.size() == 0);
        notifyDataSetChanged();
    }

    private List<Task> filterTasks(List<Task> tasks) {
        if (tasks != null){
            if (filterState.equals(FILTER_UNFINISHED)) {
                tasks.removeIf(Task::isDone);
            }
        }
        return tasks;
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TaskListEntryBinding binding = TaskListEntryBinding.inflate(LayoutInflater.from(
                parent.getContext()), parent, false);
        return new TaskViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        Task task = filteredTasks.get(position);
        holder.nameTextView.setText(task.getShortName());
        holder.taskDone.setChecked(task.isDone());
        holder.taskDone.setOnClickListener((v-> listener.onTaskStatusChange(task, !task.isDone())));
        holder.itemView.setOnClickListener(v -> listener.onTaskSelected(task));
    }

    @Override
    public int getItemCount() {
        return filteredTasks.size();
    }

    public int getAllItemCount() {
        return allTasks.size();
    }

    public static class TaskViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView;
        CheckBox taskDone;

        public TaskViewHolder(TaskListEntryBinding binding) {
            super(binding.getRoot());
            nameTextView = binding.listTaskName;
            taskDone = binding.checkBox;
        }
    }

    public void setFilterState(String filterState) {
        this.filterState = filterState;
    }

    public  interface TaskSelectionListener{
        void onTaskSelected(Task task);
        void onTaskStatusChange(Task task, boolean status);
    }

    public MutableLiveData<Boolean> getIsListEmpty() {
        return isListEmpty;
    }
}
