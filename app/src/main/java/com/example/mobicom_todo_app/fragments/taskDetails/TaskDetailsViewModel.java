package com.example.mobicom_todo_app.fragments.taskDetails;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mobicom_todo_app.model.Task;
import com.example.mobicom_todo_app.stores.TaskRepositoryImpl;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class TaskDetailsViewModel extends ViewModel {

    private final TaskRepositoryImpl repository;

    // Task attributes
    private final MutableLiveData<Long> taskId;
    private final MutableLiveData<String> taskShortName;
    private final MutableLiveData<String> taskDescription;
    private final MutableLiveData<Date> taskDueDate;
    private final MutableLiveData<String> taskDueDateHumanized;
    private final MutableLiveData<Boolean> taskDone;
    private final MutableLiveData<Long> taskListId;

    // errors
    private final MutableLiveData<String> nameError;
    private final MutableLiveData<String> dateError;

    // flags
    private final MutableLiveData<Boolean> isEditMode;
    private boolean isSplitScreen = true;

    // Try with complete task Object and then with no copy in splitMode
    // Maybe the the isDone flag will be updated

    public TaskDetailsViewModel(TaskRepositoryImpl repository) {
        this.repository = repository;
        taskId = new MutableLiveData<>();
        taskShortName = new MutableLiveData<>("");
        taskDescription = new MutableLiveData<>("");
        taskDueDate = new MutableLiveData<>();
        taskDueDateHumanized = new MutableLiveData<>("");
        taskDone = new MutableLiveData<>(false);
        taskListId = new MutableLiveData<>();
        nameError = new MutableLiveData<>();
        dateError = new MutableLiveData<>();
        isEditMode = new MutableLiveData<>(false);
    }

    public void setTask(Task task) {
        taskId.setValue(task.getMid());
        taskShortName.setValue(task.getShortName());
        taskDescription.setValue(task.getDescription());
        setTaskDate(task.getDueDate(), null);
        taskDueDate.setValue(task.getDueDate());
        taskDone.setValue(task.isDone());
        taskListId.setValue(task.getTaskListId());
    }

    public void onTextChange(CharSequence sequence) {
        if (sequence.length() > 0) {
            nameError.setValue(null);
        }
    }

    public void setTaskDate(Date date, String errorMessage) {
        taskDueDate.setValue(date);
        if (date != null) {
            if (!isDateValid(date)) {
                dateError.setValue(errorMessage);
            } else {
                // reset error state
                dateError.setValue(null);
            }
            taskDueDateHumanized.setValue(DateFormat.getDateInstance().format(date));
        } else {
            taskDueDateHumanized.setValue("");
        }
    }

    private boolean isDateValid(Date taskDate) {
        Calendar today = Calendar.getInstance();
        Calendar checkDate = Calendar.getInstance();
        checkDate.setTime(taskDate);

        return today.before(checkDate);
    }

    public void addNewTask() {
        Task task = getTask();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> repository.addSingleTask(task));
        resetTask();
    }

    public void updateTask() {
        Task task = getTask();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> repository.updateTask(task));
        resetTask();
    }

    public void deleteTask() {
        Long ID = taskId.getValue();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> repository.deleteSingleTask(ID));
        resetTask();
    }

    public void resetTask() {
        taskId.setValue(null);
        taskShortName.setValue("");
        taskDescription.setValue("");
        setTaskDate(null, null);
        taskDone.setValue(false);
        taskListId.setValue(null);
    }

    public Task getTask() {
        return new Task(taskId.getValue(), taskShortName.getValue(), taskDescription.getValue(),
                taskDueDate.getValue(), taskDone.getValue(), taskListId.getValue());
    }

    public MutableLiveData<Long> getTaskId() {
        return taskId;
    }

    public MutableLiveData<String> getTaskShortName() {
        return taskShortName;
    }

    public MutableLiveData<String> getTaskDescription() {
        return taskDescription;
    }

    public MutableLiveData<Date> getTaskDueDate() {
        return taskDueDate;
    }

    public MutableLiveData<String> getTaskDueDateHumanized() {
        return taskDueDateHumanized;
    }

    public MutableLiveData<Boolean> getTaskDone() {
        return taskDone;
    }

    public MutableLiveData<Long> getTaskListId() {
        return taskListId;
    }

    public MutableLiveData<String> getNameError() {
        return nameError;
    }

    public MutableLiveData<String> getDateError() {
        return dateError;
    }

    public MutableLiveData<Boolean> getIsEditMode() {
        return isEditMode;
    }

    public boolean isSplitScreen() {
        return isSplitScreen;
    }

    public void setSplitScreen(boolean splitScreen) {
        isSplitScreen = splitScreen;
    }
}
