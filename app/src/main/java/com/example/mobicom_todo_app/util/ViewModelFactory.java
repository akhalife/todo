package com.example.mobicom_todo_app.util;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.mobicom_todo_app.fragments.taskDetails.TaskDetailsViewModel;
import com.example.mobicom_todo_app.fragments.taskList.TaskListViewModel;
import com.example.mobicom_todo_app.stores.TaskRepositoryImpl;

public class ViewModelFactory implements ViewModelProvider.Factory {
    private final TaskRepositoryImpl repository;

    public ViewModelFactory(TaskRepositoryImpl repository) {
        this.repository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(TaskDetailsViewModel.class)) {
            return (T) new TaskDetailsViewModel(repository);
        } else if (modelClass.isAssignableFrom(TaskListViewModel.class)) {
            return (T) new TaskListViewModel(repository);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
