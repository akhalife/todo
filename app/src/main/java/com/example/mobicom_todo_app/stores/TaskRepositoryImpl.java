package com.example.mobicom_todo_app.stores;

import androidx.lifecycle.LiveData;

import com.example.mobicom_todo_app.MyTasksApp;
import com.example.mobicom_todo_app.database.TaskDao;
import com.example.mobicom_todo_app.database.TaskRoomDatabase;
import com.example.mobicom_todo_app.model.TaskList;
import com.example.mobicom_todo_app.repositories.TaskRepository;
import com.example.mobicom_todo_app.model.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thorsten on 23.03.20.
 * Edited by mvoigt and akalife
 */

public class TaskRepositoryImpl implements TaskRepository {

    private static TaskRepositoryImpl instance;

    private TaskRoomDatabase db;
    private TaskDao dao;

    private final List<Task> mTasks;
    private final List<TaskList> mTaskLists;

    public static synchronized TaskRepositoryImpl getInstance() {
        if (instance == null) {
            instance = new TaskRepositoryImpl();
        }
        return instance;
    }

    private TaskRepositoryImpl() {
        db = MyTasksApp.appDatabase;
        dao = db.taskDao();

        mTasks = new ArrayList<>();
        mTaskLists = new ArrayList<>();
    }

    @Override
    public LiveData<List<Task>> loadTasks() {
        return dao.getAllTasks();
    }

    @Override
    public void deleteFinishedTasks() {
        dao.deleteFinishedTasks();
    }

    @Override
    public void deleteSingleTask(Long mTaskId) {
        dao.deleteSingleTask(mTaskId);
    }


    @Override
    public void addSingleTask(Task mTask) {
        dao.addTask(mTask);
    }

    @Override
    public void addTasks(Task... mTasks) {
        for (Task task : mTasks) {
            this.addSingleTask(task);
        }
    }

    @Override
    public void updateTask(Task mTask) {
        // mTasks.set(mTasks.indexOf(mTask), mTask);
        dao.updateTask(mTask);
    }

    @Override
    public List<TaskList> loadTaskLists() {
        return mTaskLists;
    }

    @Override
    public void addTaskList(TaskList mTaskList) {
        this.mTaskLists.add(mTaskList);
    }

    @Override
    public void deleteTaskList(int mTaskListId) {
        this.mTaskLists.removeIf((list) -> list.getListId() == mTaskListId);
    }

    @Override
    public void addTaskToList(Task mTask, TaskList mTaskList) {
        TaskList list = this.mTaskLists.get(this.mTaskLists.indexOf(mTaskList));
        list.addTask(mTasks.remove(mTasks.indexOf(mTask)));
    }

    @Override
    public void removeTaskFromList(Task mTask, TaskList mTaskList) {
        TaskList list = this.mTaskLists.get(this.mTaskLists.indexOf(mTaskList));
        this.mTasks.add(list.removeTask(mTask));
    }
}
