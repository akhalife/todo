package com.example.mobicom_todo_app.fragments.taskList;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mobicom_todo_app.model.Task;
import com.example.mobicom_todo_app.stores.TaskRepositoryImpl;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaskListViewModel extends ViewModel {

    private final TaskRepositoryImpl repository;
    private LiveData<List<Task>> databaseTasks;
    private final MutableLiveData<String> filterState;

    public TaskListViewModel(TaskRepositoryImpl repository) {
        this.repository = repository;
        filterState = new MutableLiveData<>();
    }

    public LiveData<List<Task>> getDatabaseTasks() {
        if (databaseTasks == null) {
            databaseTasks = repository.loadTasks();
        }
        return databaseTasks;
    }

    public void onDeleteFinishedTasks() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(repository::deleteFinishedTasks);
    }

    public void onTaskStatusChange(Task task, boolean status) {
        task.setDone(status);
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> repository.updateTask(task));
    }

    public MutableLiveData<String> getFilterState() {
        return filterState;
    }
}
